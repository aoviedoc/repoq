package Oviedo.Adrian.bl;

public class Cuenta {
    private String nombre;
    private String apellidos;
    private int identificacion;
    private String direccionExacta;
    private String usuario;
    private Provincia provincia;

    @Override
    public String toString() {
        return "Cuenta{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", identificacion=" + identificacion +
                ", direccionExacta='" + direccionExacta + '\'' +
                ", usuario='" + usuario + '\'' +
                ", provincia=" + provincia +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getDireccionExacta() {
        return direccionExacta;
    }

    public void setDireccionExacta(String direccionExacta) {
        this.direccionExacta = direccionExacta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Cuenta(String nombre, String apellidos, int identificacion, String direccionExacta, String usuario, Provincia provincia) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.identificacion = identificacion;
        this.direccionExacta = direccionExacta;
        this.usuario = usuario;
        this.provincia = provincia;
    }

    public Cuenta() {
    }
}
