package Oviedo.Adrian.bl;

public class Canton {
    private String nombre;
    private int codigo;
    private Provincia provincia;

    @Override
    public String toString() {
        return "Canton{" +
                "nombre='" + nombre + '\'' +
                ", codigo=" + codigo +
                ", provincia=" + provincia +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Canton(String nombre, int codigo, Provincia provincia) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.provincia = provincia;
    }

    public Canton() {
    }
}
