package Oviedo.Adrian.bl;

import java.util.ArrayList;

public class Conductor extends Cuenta {
    private String imagen;
    private String clave;
    private ArrayList<Pedido> pedidos;

    public Conductor(String nombre, String apellidos, int identificacion, String direccionExacta, String usuario, Provincia provincia, String imagen, String clave, ArrayList<Pedido> pedidos) {
        super(nombre, apellidos, identificacion, direccionExacta, usuario, provincia);
        this.imagen = imagen;
        this.clave = clave;
        this.pedidos = pedidos;
    }

    public Conductor() {
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ArrayList<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public String toString() {
        return "Conductor{" +
                "imagen='" + imagen + '\'' +
                ", clave='" + clave + '\'' +
                ", cuenta='" + super.toString() + '\'' +
                ", pedidos=" + pedidos +
                '}';
    }
}
