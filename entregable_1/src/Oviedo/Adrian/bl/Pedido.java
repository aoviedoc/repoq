package Oviedo.Adrian.bl;

import java.util.Date;

public class Pedido {
    private int numeroPedido;
    private Date fechaCreacion;
    private String direccionEntrega;
    private MetodosDePago metodosDePago;

    public Pedido() {
    }

    public Pedido(int numeroPedido, Date fechaCreacion, String direccionEntrega, MetodosDePago metodosDePago) {
        this.numeroPedido = numeroPedido;
        this.fechaCreacion = fechaCreacion;
        this.direccionEntrega = direccionEntrega;
        this.metodosDePago = metodosDePago;
    }

    public int getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(int numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getDireccionEntrega() {
        return direccionEntrega;
    }

    public void setDireccionEntrega(String direccionEntrega) {
        this.direccionEntrega = direccionEntrega;
    }

    public MetodosDePago getMetodosDePago() {
        return metodosDePago;
    }

    public void setMetodosDePago(MetodosDePago metodosDePago) {
        this.metodosDePago = metodosDePago;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "numeroPedido=" + numeroPedido +
                ", fechaCreacion=" + fechaCreacion +
                ", direccionEntrega='" + direccionEntrega + '\'' +
                ", metodosDePago=" + metodosDePago +
                '}';
    }
}
