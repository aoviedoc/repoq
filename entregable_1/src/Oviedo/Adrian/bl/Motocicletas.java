package Oviedo.Adrian.bl;

public class Motocicletas extends MediosTransporte{
    private String tipo;

    public Motocicletas(String marca, int placa, String tipo) {
        super(marca, placa);
        this.tipo = tipo;
    }

    public Motocicletas() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Motocicletas{" +
                "tipo='" + tipo + '\'' +
                "Medios de Transporte='" + super.toString() + '\'' +
                '}';
    }
}
