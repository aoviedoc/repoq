package Oviedo.Adrian.bl;

import java.util.ArrayList;
import java.util.Date;

public class Cliente extends Cuenta {
    private String imagen;
    private Date fechaNacimiento;
    private int edad;
    private String genero;
    private int telefono;
    private String clave;
    private MetodosDePago metodosDePago;
    private ArrayList<Pedido> pedidos;
    private ArrayList<MediosTransporte> mediosTransporte;
    private ArrayList<Servicios> servicios;

    public Cliente(String nombre, String apellidos, int identificacion, String direccionExacta, String usuario, Provincia provincia, String imagen, Date fechaNacimiento, int edad, String genero, int telefono, String clave, MetodosDePago metodosDePago, ArrayList<Pedido> pedidos, ArrayList<MediosTransporte> mediosTransporte, ArrayList<Servicios> servicios) {
        super(nombre, apellidos, identificacion, direccionExacta, usuario, provincia);
        this.imagen = imagen;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.genero = genero;
        this.telefono = telefono;
        this.clave = clave;
        this.metodosDePago = metodosDePago;
        this.pedidos = pedidos;
        this.mediosTransporte = mediosTransporte;
        this.servicios = servicios;
    }

    public Cliente() {
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public MetodosDePago getMetodosDePago() {
        return metodosDePago;
    }

    public void setMetodosDePago(MetodosDePago metodosDePago) {
        this.metodosDePago = metodosDePago;
    }

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ArrayList<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public ArrayList<MediosTransporte> getMediosTransporte() {
        return mediosTransporte;
    }

    public void setMediosTransporte(ArrayList<MediosTransporte> mediosTransporte) {
        this.mediosTransporte = mediosTransporte;
    }

    public ArrayList<Servicios> getServicios() {
        return servicios;
    }

    public void setServicios(ArrayList<Servicios> servicios) {
        this.servicios = servicios;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "imagen='" + imagen + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", genero='" + genero + '\'' +
                ", telefono=" + telefono +
                ", clave='" + clave + '\'' +
                ", metodosDePago=" + metodosDePago +
                ", pedidos=" + pedidos +
                ", mediosTransporte=" + mediosTransporte +
                ", servicios=" + servicios +
                ", Cuenta=" + super.toString() +
                '}';
    }
}
