package Oviedo.Adrian.bl;

public class Servicios {
    private int codigoServicio;
    private String descripcion;
    private double precioBase;
    private String tipo;
    private String estado;
    private Gasolina gasolina;

    public Servicios() {
    }

    public Servicios(int codigoServicio, String descripcion, double precioBase, String tipo, String estado, Gasolina gasolina) {
        this.codigoServicio = codigoServicio;
        this.descripcion = descripcion;
        this.precioBase = precioBase;
        this.tipo = tipo;
        this.estado = estado;
        this.gasolina = gasolina;
    }

    public int getCodigoServicio() {
        return codigoServicio;
    }

    public void setCodigoServicio(int codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(double precioBase) {
        this.precioBase = precioBase;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }

    @Override
    public String toString() {
        return "Servicios{" +
                "codigoServicio=" + codigoServicio +
                ", descripcion='" + descripcion + '\'' +
                ", precioBase=" + precioBase +
                ", tipo='" + tipo + '\'' +
                ", estado='" + estado + '\'' +
                ", gasolina=" + gasolina +
                '}';
    }
}
