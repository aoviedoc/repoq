package Oviedo.Adrian.bl;

public class Distrito {
    private String nombre;
    private int codigo;
    private Canton canton;

    public Distrito() {
    }

    @Override
    public String toString() {
        return "Distrito{" +
                "nombre='" + nombre + '\'' +
                ", codigo=" + codigo +
                ", canton=" + canton +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Canton getCanton() {
        return canton;
    }

    public void setCanton(Canton canton) {
        this.canton = canton;
    }

    public Distrito(String nombre, int codigo, Canton canton) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.canton = canton;
    }
}
