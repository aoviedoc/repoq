package Oviedo.Adrian.bl;

import java.util.Date;

public class MetodosDePago {
    private int numeroTarjeta;
    private String provedor;
    private Date fechaExpiracion;
    private  int codigoSeguridad;
    private String nombre;


    @Override
    public String toString() {
        return "MetodosDePago{" +
                "numeroTarjeta=" + numeroTarjeta +
                ", provedor='" + provedor + '\'' +
                ", fechaExpiracion=" + fechaExpiracion +
                ", codigoSeguridad=" + codigoSeguridad +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public int getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(int numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getProvedor() {
        return provedor;
    }

    public void setProvedor(String provedor) {
        this.provedor = provedor;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public int getCodigoSeguridad() {
        return codigoSeguridad;
    }

    public void setCodigoSeguridad(int codigoSeguridad) {
        this.codigoSeguridad = codigoSeguridad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public MetodosDePago(int numeroTarjeta, String provedor, Date fechaExpiracion, int codigoSeguridad, String nombre) {
        this.numeroTarjeta = numeroTarjeta;
        this.provedor = provedor;
        this.fechaExpiracion = fechaExpiracion;
        this.codigoSeguridad = codigoSeguridad;
        this.nombre = nombre;
    }

    public MetodosDePago() {
    }
}
