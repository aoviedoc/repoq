package Oviedo.Adrian.bl;

public class Gasolina {
    private double precioXLitro;

    @Override
    public String toString() {
        return "Gasolina{" +
                "precioXLitro=" + precioXLitro +
                '}';
    }

    public double getPrecioXLitro() {
        return precioXLitro;
    }

    public void setPrecioXLitro(double precioXLitro) {
        this.precioXLitro = precioXLitro;
    }

    public Gasolina(double precioXLitro) {
        this.precioXLitro = precioXLitro;
    }

    public Gasolina() {
    }
}
