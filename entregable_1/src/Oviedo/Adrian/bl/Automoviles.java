package Oviedo.Adrian.bl;

public class Automoviles extends MediosTransporte{
    private String tipo;
    private int anno;

    public Automoviles(String marca, int placa, String tipo, int anno) {
        super(marca, placa);
        this.tipo = tipo;
        this.anno = anno;
    }

    public Automoviles() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    @Override
    public String toString() {
        return "Automoviles{" +
                "tipo='" + tipo + '\'' +
                "Medio de Transporte='" + super.toString() + '\'' +
                ", anno=" + anno +
                '}';
    }
}
