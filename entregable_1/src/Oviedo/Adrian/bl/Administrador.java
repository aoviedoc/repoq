package Oviedo.Adrian.bl;

public class Administrador extends Cuenta {
    private String clave;

    public Administrador(String nombre, String apellidos, int identificacion, String direccionExacta, String usuario, Provincia provincia, String clave) {
        super(nombre, apellidos, identificacion, direccionExacta, usuario, provincia);
        this.clave = clave;
    }

    public Administrador() {
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public String toString() {
        return "Administrador{" +
                "clave='" + clave + '\'' +
                "Cuenta='" + super.toString() + '\'' +
                '}';
    }
}

