package Oviedo.Adrian.bl;

public class MediosTransporte {
    private String marca;
    private int placa;


    @Override
    public String toString() {
        return "mediosTransporte{" +
                "marca='" + marca + '\'' +
                ", placa=" + placa +
                '}';
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getPlaca() {
        return placa;
    }

    public void setPlaca(int placa) {
        this.placa = placa;
    }

    public MediosTransporte(String marca, int placa) {
        this.marca = marca;
        this.placa = placa;
    }

    public MediosTransporte() {
    }
}
