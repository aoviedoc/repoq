package Oviedo.Adrian.ui;

import java.io.*;
public class ui {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    public static void main(String[] args) throws IOException{
        // write your code here
        int opcion= -1;
        inicializarPrograma();//esto es opcional
        do{
            mostrarMenu();
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        }while (opcion!=0);
    }
    static void mostrarMenu(){
        //TODO: llenar las opciones de menú según el programa a desarrollar.
        out.println("1.");
        out.println("2.");
        out.println("3.");
        out.println("0.Salir");

    }
    static void procesarOpcion(int pOpcion){
        switch (pOpcion){
            case 1:
                break;
            case 2:
                break;
            case 0:
                out.println("Gracias por usar el programa");
                break;
            default:
                out.println("Opción inválida");
                break;
        }
    }
    static void inicializarPrograma(){

    }
    static int seleccionarOpcion() throws IOException{
        out.println("Digite la opcion");
        return Integer.parseInt(in.readLine());
    }

}